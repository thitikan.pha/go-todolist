package controller

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/hgcassiopeia/todo-list/todo"
)

type TodoController struct {
	Tasks *todo.TaskList // TaskList is a struct that contains a list of tasks
}

func New(td *TodoController) *gin.Engine {

	r := gin.Default()
	r.GET("/", td.GetTodo)
	r.GET("/:status", td.TodoStatusTrue)
	r.POST("/", td.AddTodo)
	r.DELETE("/:index", td.DeleteTodo)

	return r
}

func (tdController *TodoController) DeleteTodo(c *gin.Context) {
	index := c.Param("index")
	position, _ := strconv.ParseInt(index, 6, 8)

	tdController.Tasks.Remove(int(position))

	c.JSON(http.StatusOK, gin.H{
		"list": tdController.Tasks,
	})
}

func (tdController *TodoController) AddTodo(c *gin.Context) {
	taskName := c.PostForm("task-name")

	tdController.Tasks.Add(taskName)

	c.JSON(http.StatusOK, gin.H{
		"list": tdController.Tasks,
	})
}

func (tdController *TodoController) TodoStatusTrue(c *gin.Context) {
	status := c.Param("status")
	var tmpStatus bool
	tmpStatus, _ = strconv.ParseBool(status)
	tmpTask := tdController.Tasks.FindStatus(tmpStatus)

	c.JSON(http.StatusOK, gin.H{
		"list": tmpTask,
	})
}
