package main

import (
	"github.com/hgcassiopeia/todo-list/controller"
	"github.com/hgcassiopeia/todo-list/dbservice/sqlite"
	"github.com/hgcassiopeia/todo-list/todo"
)

func main() {
	// Load config
	// initilize database

	todoController := controller.New(&controller.TodoController{
		Tasks: &todo.TaskList{
			DB: &sqlite.DB{},
		},
	})

	todoController.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
