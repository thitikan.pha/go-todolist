package dbservice

import "github.com/hgcassiopeia/todo-list/task"

//go:generate mockery --name=Database
type Database interface {
	Save([]*task.Task)
	GetAll() []*task.Task
}
