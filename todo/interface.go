package todo

import "github.com/hgcassiopeia/todo-list/dbservice"

func New(db dbservice.Database) *TaskList {
	return &TaskList{
		DB: db,
	}
}
