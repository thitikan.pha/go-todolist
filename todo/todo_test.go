package todo

import (
	"testing"

	"github.com/hgcassiopeia/todo-list/dbservice/mocks"
	"github.com/hgcassiopeia/todo-list/task"
	"github.com/test-go/testify/assert"
)

func TestFindStatus(t *testing.T) {
	// Arrange
	dummyDB := mocks.NewDatabase(t)
	tasks := New(dummyDB)

	mockTask := []*task.Task{
		{
			TaskName:   "test",
			TaskStatus: true,
		},
	}

	dummyDB.On("GetAll").Return(mockTask)

	// Act
	result := tasks.FindStatus(true)

	//Assert
	assert.Equal(t, 1, len(result))
	assert.True(t, result[0].TaskStatus)
}

// type stubDB struct{}

// func (s *stubDB) GetAll() []*task.Task {
// 	return []*task.Task{
// 		{
// 			TaskName:   "A1",
// 			TaskStatus: false,
// 		},
// 	}

// }

// func (s *stubDB) Save([]*task.Task) {}

func TestAdd(t *testing.T) {
	// Arrange
	dummyDB := mocks.NewDatabase(t)
	tasks := New(dummyDB)

	mockTask := []*task.Task{
		{
			TaskName:   "test",
			TaskStatus: true,
		},
	}

	dummyDB.On("GetAll").Return(mockTask)
	dummyDB.On("Save", []*task.Task{
		{
			TaskName:   "test",
			TaskStatus: true,
		},
		{
			TaskName:   "A2",
			TaskStatus: false,
		},
	}).Return(mockTask)

	// Act
	tasks.Add("A2")
}

func TestRemove(t *testing.T) {
	// Arrange
	dummyDB := mocks.NewDatabase(t)
	tasks := New(dummyDB)

	mockTask := []*task.Task{
		{
			TaskName:   "A1",
			TaskStatus: true,
		},
		{
			TaskName:   "A2",
			TaskStatus: true,
		},
	}

	dummyDB.On("GetAll").Return(mockTask)
	dummyDB.On("Save", []*task.Task{
		{
			TaskName:   "A2",
			TaskStatus: true,
		},
	}).Return(mockTask)

	// Act : Remove first position
	tasks.Remove(0)

	// Assert
	// assert.Len(t, tasks.Items, 1)                 // after remove should be only 1 task
	// assert.Equal(t, expectedTask, tasks.Items[0]) // that task should be A2
}
