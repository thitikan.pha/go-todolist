package todo

import (
	"github.com/hgcassiopeia/todo-list/dbservice"
	"github.com/hgcassiopeia/todo-list/task"
)

type TaskList struct {
	// Items []*Task
	DB dbservice.Database
}

func (t *TaskList) FindStatus(status bool) []*task.Task {
	tmpTask := []*task.Task{}

	items := t.DB.GetAll()

	for _, v := range items {
		if status == v.TaskStatus {
			tmpTask = append(tmpTask, v)
		}
	}

	return tmpTask
}

func (t *TaskList) Add(taskName string) {
	items := t.DB.GetAll()

	items = append(items, &task.Task{
		TaskName:   taskName,
		TaskStatus: false,
	})

	t.DB.Save(items)
}

func (t *TaskList) Remove(s int) {
	items := t.DB.GetAll()

	items = append(items[:s], items[s+1:]...)

	t.DB.Save(items)
}
